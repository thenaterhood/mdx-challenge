package com.mindex.challenge.service.impl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import com.mindex.challenge.data.Compensation;
import com.mindex.challenge.data.Employee;
import com.mindex.challenge.service.CompensationService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CompensationServiceImplTest {

    private String compensationUrl;
    private String testEmployeeId;

    @Autowired
    private CompensationService compensationService;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void setup() {    
        compensationUrl = "http://localhost:" + port + "/employee/{id}/compensation";
        testEmployeeId = "16a596ae-edd3-4847-99fe-c4518e82c86f";
    }

    @Test(expected = RuntimeException.class)
	public void testInvalidEmployee() {

        compensationService.readLatest("foo");
    }

    @Test(expected = RuntimeException.class)
	public void testNoCompensationForEmployee() {

        compensationService.readLatest(testEmployeeId);
    }

    @Test
    public void testCreateRead() {

        Date now = new Date();
        Compensation newCompensation = new Compensation();
        newCompensation.setEffectiveDate(now);
        newCompensation.setSalary(new BigDecimal(2000));

        Employee e = new Employee();
        e.setEmployeeId(testEmployeeId);
        newCompensation.setEmployee(e);

        Compensation createdCompensation = restTemplate.postForEntity(compensationUrl, newCompensation, Compensation.class, testEmployeeId).getBody();
        assertNotNull(createdCompensation.getEmployee());
        assertEquals(testEmployeeId, createdCompensation.getEmployee().getEmployeeId());

        Compensation r = restTemplate.getForEntity(compensationUrl, Compensation.class, testEmployeeId).getBody();
        assertEquals(newCompensation.getSalary(), r.getSalary());
        // A basic check to make sure we got back a non-null employee
        assertNotNull(r.getEmployee());
    }

    @Test
    public void testReadLatestReturnsLatest() {
        // If we have multiple Compensations, we expect to only retrieve the
        // most recent regardless of the order they were created in.

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
        Date firstDate = null;
        Date secondDate = null;
        Date thirdDate = null;
        Compensation newCompensation = new Compensation();

        newCompensation.setSalary(new BigDecimal(4000));

        Employee employee = new Employee();
        employee.setEmployeeId(testEmployeeId);
        newCompensation.setEmployee(employee);

        try {
            firstDate = sdf.parse("2022-04-05");
        } catch (ParseException e) {
            assertFalse(true);
        }

        newCompensation.setEffectiveDate(firstDate);
 
        restTemplate.postForEntity(compensationUrl, newCompensation, Compensation.class, testEmployeeId).getBody();

        try {
            secondDate = sdf.parse("2020-04-05");
        } catch (ParseException e) {
            assertFalse(true);
        }

        newCompensation.setEffectiveDate(secondDate);
        newCompensation.setSalary(new BigDecimal(1000));
        restTemplate.postForEntity(compensationUrl, newCompensation, Compensation.class, testEmployeeId).getBody();

        try {
            thirdDate = sdf.parse("2021-04-05");
        } catch (ParseException e) {
            assertFalse(true);
        }

        newCompensation.setEffectiveDate(thirdDate);
        newCompensation.setSalary(new BigDecimal(2000));
        restTemplate.postForEntity(compensationUrl, newCompensation, Compensation.class, testEmployeeId).getBody();

        Compensation r = restTemplate.getForEntity(compensationUrl, Compensation.class, testEmployeeId).getBody();
        assertEquals(new BigDecimal(4000), r.getSalary());
        assertEquals(firstDate, r.getEffectiveDate());
        // A basic check to make sure we got back a non-null employee
        assertNotNull(r.getEmployee());
    }
}
