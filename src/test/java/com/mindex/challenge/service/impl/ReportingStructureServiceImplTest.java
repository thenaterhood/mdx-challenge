package com.mindex.challenge.service.impl;

import com.mindex.challenge.data.ReportingStructure;
import com.mindex.challenge.service.ReportingStructureService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReportingStructureServiceImplTest {

    private String employeeReportStructureUrl;

    @Autowired
    private ReportingStructureService reportingStructureService;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void setup() {
        employeeReportStructureUrl = "http://localhost:" + port + "/employee/{id}/reporting_structure";
    }

    @Test
	public void testNoReports() {
        ReportingStructure r = restTemplate.getForEntity(employeeReportStructureUrl, ReportingStructure.class, "b7839309-3348-463b-a7e3-5de1c168beb3").getBody();
        assertEquals(0, r.getNumberOfReports());
        // A basic check to make sure we got back a non-null employee
        // This check could be fleshed out more, but we already have
        // tests for the EmployeeService
        assertNotNull(r.getEmployee());
	}

	@Test
	public void testMultipleTiers() {
		// Tests that we correctly count reports if there are multiple
		// tiers of reports
        ReportingStructure r = restTemplate.getForEntity(employeeReportStructureUrl, ReportingStructure.class, "16a596ae-edd3-4847-99fe-c4518e82c86f").getBody();
        assertEquals(4, r.getNumberOfReports());
        // A basic check to make sure we got back a non-null employee
        assertNotNull(r.getEmployee());
	}

	@Test(expected = RuntimeException.class)
	public void testInvalidEmployee() {

        reportingStructureService.read("foo");
    }
}
