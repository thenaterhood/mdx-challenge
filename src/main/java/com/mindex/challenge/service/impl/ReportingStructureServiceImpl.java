package com.mindex.challenge.service.impl;

import com.mindex.challenge.dao.EmployeeRepository;
import com.mindex.challenge.data.Employee;
import com.mindex.challenge.data.ReportingStructure;
import com.mindex.challenge.service.ReportingStructureService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;

@Service
public class ReportingStructureServiceImpl implements ReportingStructureService {

    private static final Logger LOG = LoggerFactory.getLogger(ReportingStructureServiceImpl.class);
    // 100 is an arbitrarily chosen limit
    private static final int MAX_REPORTING_STRUCTURE_DEPTH = 100;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public ReportingStructure read(String employeeId) {

        LOG.debug("Reading reporting structure for employee with id [{}]", employeeId);

        Employee employee = this.employeeRepository.findByEmployeeId(employeeId);
        if (employee == null) {
            throw new RuntimeException("Invalid employeeId: " + employeeId);
        }

        HashSet<String> reportIds = this.getReports(employee, new HashSet<String>(), 0);

        // The report IDs set contains the start employee, so subtract one
        int reportCount = reportIds.size() - 1;
        LOG.debug("Found [{}] reports under employee ID [{}]", reportCount, employee.getEmployeeId());

        return new ReportingStructure(employee, reportCount);
    }

    private HashSet<String> getReports(Employee start, HashSet<String> seenEmployeeIds, int depth) {

        LOG.debug("Getting reports for employee with id [{}]", start.getEmployeeId());

        seenEmployeeIds.add(start.getEmployeeId());

        // Provide an option for limiting the recursion depth.
        // While this isn't needed for the example data, unbounded recursion
        // can be dangerous for resource usage with data that might be untrusted
        // or more complex than anticipated.
        // If reaching this limit were happening in a Production system I might
        // explore moving this to a periodic job and caching the result(s).
        if (depth > ReportingStructureServiceImpl.MAX_REPORTING_STRUCTURE_DEPTH) {
            LOG.debug("Max search depth of [{}] reached for reporting structure", ReportingStructureServiceImpl.MAX_REPORTING_STRUCTURE_DEPTH);
            return seenEmployeeIds;
        }

        List<Employee> directReports = start.getDirectReports();
        if (directReports == null) {
            return seenEmployeeIds;
        }

        for (Employee directReport: directReports) {

            if (seenEmployeeIds.contains(directReport.getEmployeeId())) {
                continue;
            }

            // Depending on the specifics of the data access layer, this
            // could pose a scalability concern if there were a deep reporting tree
            // due to the number of queries/requests.
            Employee e = this.employeeRepository.findByEmployeeId(directReport.getEmployeeId());

            this.getReports(e, seenEmployeeIds, depth + 1);
        }

        return seenEmployeeIds;
    }
}
