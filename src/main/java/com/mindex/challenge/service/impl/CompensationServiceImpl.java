package com.mindex.challenge.service.impl;

import com.mindex.challenge.dao.CompensationRepository;
import com.mindex.challenge.dao.EmployeeRepository;
import com.mindex.challenge.data.Compensation;
import com.mindex.challenge.data.Employee;
import com.mindex.challenge.service.CompensationService;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompensationServiceImpl implements CompensationService {

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Autowired
    private CompensationRepository compensationRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Compensation create(Compensation compensation) {
        LOG.debug("Creating compensation [{}]", compensation);

        Employee employee = compensation.getEmployee();
        if (employee == null) {
            throw new RuntimeException("Compensation is missing an employee");
        }

        String employeeId = employee.getEmployeeId();

        Employee storedEmployee = employeeRepository.findByEmployeeId(employeeId);
        if (storedEmployee == null) {
            throw new RuntimeException("Invalid employee with id: " + employeeId);
        }

        compensationRepository.insert(compensation);
        compensation.setEmployee(storedEmployee);

        return compensation;
    }

    @Override
    public Compensation readLatest(String employeeId) {
        LOG.debug("Reading compensation for employee with id [{}]", employeeId);

        Employee employee = employeeRepository.findByEmployeeId(employeeId);
        if (employee == null) {
            throw new RuntimeException("Invalid employeeId: " + employeeId);
        }

        List<Compensation> compensations = compensationRepository.findByEmployee(employee);

        if (compensations == null || compensations.isEmpty()) {
            throw new RuntimeException("Employee with ID '" + employeeId + "' does not have compensation");
        }

        // I'm choosing to return a single compensation which is the most recent.
        // While returning a whole history could be useful, the data structure
        // (or my interaction with an unfamiliar datastore) isn't ideal because
        // it results in the endpoint returning compensations that each include
        // a copy of the Employee - which is repetitive and means the returned data
        // is much larger than it needs to be.
        compensations.sort(new java.util.Comparator<Compensation>() {
            public int compare(Compensation a, Compensation b) {
                return b.getEffectiveDate().compareTo(a.getEffectiveDate());
            }});

        for (Compensation c : compensations) {
            c.setEmployee(employee);
        }

        return compensations.get(0);
    }
}
